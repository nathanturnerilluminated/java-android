package opensecurity.webviewignoressl;

import android.net.http.SslError;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;
import android.content.pm.PackageManager;


public class MainActivity extends ActionBarActivity {
public WebView  webView;
    public Button bt;
    //stat="True" - Allow insecure SSL
    //stat='False" - Block insecure SSL

    public static Boolean stat=Boolean.FALSE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String url= "https://tv.eurosport.com/";
        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.clearHistory();
        webView.clearFormData();
        webView.clearCache(true);
        webView.setWebViewClient(
                new SSLTolerentWebViewClient()
        );

        webView.loadUrl(url);


    }
    private class SSLTolerentWebViewClient extends WebViewClient {

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {

            if (MainActivity.stat)
            {
                handler.proceed(); // Ignore SSL certificate errors
            Toast.makeText(getBaseContext(), (String) "Allowing Insecure SSL",
                    Toast.LENGTH_SHORT).show();
        }
            else {
                handler.cancel();
                Toast.makeText(getBaseContext(), (String) "Insecure SSL - Blocked",
                        Toast.LENGTH_SHORT).show();

            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
